/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js

*/	
// --------------> ACTIVITY SOLUTION <--------------
// NUMBER 1.1
db.users.insertMany([
		{
			firstName: "Zeref",
			lastName: "Dragneel",
			email: "zd@mail.com",
			password: "zeref123",
			isAdmin: false			
		},
		{
			firstName: "Mavis",
			lastName: "Vermillion",
			email: "mv@mail.com",
			password: "mavis123",
			isAdmin: false			
		},
		{
			firstName: "Mirajane",
			lastName: "Strauss",
			email: "ms@mail.com",
			password: "mirajane123",
			isAdmin: false			
		},
		{
			firstName: "Jellal",
			lastName: "Fernandes",
			email: "jf@mail.com",
			password: "jellal123",
			isAdmin: false			
		},
		{
			firstName: "Erza",
			lastName: "Scarlet",
			email: "es@mail.com",
			password: "erza123",
			isAdmin: false			
		}
		
	])

// NUMBER 1.2
db.courses.insertMany([
		{
			name: "Celestial Spirit Magic 101",
			price: "1000",
			isActive: false
		},
		{
			name: "Dragon Slayer Magic 101",
			price: "2000",
			isActive: false
		},
		{
			name: "Re-equip Magic 101",
			price: "3000",
			isActive: false
		}
	])

// NUMBER 2
db.users.find({isAdmin: false})

// NUMBER 3.1
db.users.updateOne({}, 
	{
		$set:
		{
			isAdmin : true
		}
	})

// NUMBER 3.2
db.courses.updateOne({name: "Dragon Slayer Magic 101"}, 
			{
				$set:
				{
					isActive : true
				}
			})

// NUMBER 4
db.courses.deleteMany({isActive: false})